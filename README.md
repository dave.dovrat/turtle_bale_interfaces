# turtle_bale_interfaces

The specific interfaces required for the [turtle_bale](https://gitlab.com/dave.dovrat/turtle_bale) package.

## Usage

This repo should be cloned alongside the turtle_bale repo, such that if you are in your ROS workspace and perform the command:

```console
user@ubuntu:~/turtle_bale_ws# ls src
```

the result would be these two directories:

```console
turtle_bale
turtle_bale_interfaces
```
